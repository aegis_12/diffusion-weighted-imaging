
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <nifti1_io.h>
#include "solver.h"

#define NUM_MAX_NOBS 30

__cudaFunction__
real norm(real *x, int nObs)
{
	long int ix, nn, iincx;
	real norm, scale, absxi, ssq, temp;

	nn = nObs;
	iincx = 1;

	if( nn > 0 && iincx > 0 )
	{
    	if (nn == 1)
    	{
      		norm = fabs(x[0]);
    	}  
    	else
    	{
      		scale = 0.0;
      		ssq = 1.0;

      		for (ix=(nn-1)*iincx; ix>=0; ix-=iincx)
      		{
        		if (x[ix] != 0.0)
        		{
          			absxi = fabs(x[ix]);
          			if (scale < absxi)
          			{
            			temp = scale / absxi;
            			ssq = ssq * (temp * temp) + 1.0;
            			scale = absxi;
          			}
          			else
          			{
            			temp = absxi / scale;
            			ssq += temp * temp;
          			}
        		}
      		}
      		norm = scale * sqrt(ssq);
    	}
  }
  else
  	norm = 0.0;

  return norm;

}

__cudaFunction__
void printVector(real *v, int n)
{
	int i;

	for (i=0; i<n; i++)
		printf("%f ", v[i]);

	printf("\n");
}

__cudaFunction__
void printMatrix(real *v, int m, int n)
{
	int i, j;
	for (i=0; i<m; i++){
		for (j=0; j<n; j++){
			printf("%f ", v[i+m*j]);
		}
		printf("\n");
	}
}

/**
* Devuelve beta y se sobreescribe el vector v
* x es una de las columnas de la matriz. N es el tamaño
* de ese 'vector'
*/
__cudaFunction__
real householder_vector(real *v, real *xA, int n) 
{
	
	int i;
	real beta = 0.0, alpha=0, uta=0, xAux;

	real xNorm = norm(xA, n);

	v[0] = 1;

	for (i=0; i<n; i++)
		v[i] = xA[i] / xNorm;

	real xFirst = v[0];

	/* Calcular el valor de alpha x(2:n)' * x(2:n) */
	for (i=1; i<n; i++)
	{
		alpha += v[i] * v[i];
	}

	if (alpha == 0)
	{
		return 0;
	}
	else 
	{
		uta = sqrt(xFirst*xFirst + alpha);

		if (xFirst <= 0){
			v[0] = xFirst - uta;
		} 
		else {
			v[0] = -alpha / (xFirst+uta);
		}

		beta = 2 * (v[0]*v[0]) / (alpha + v[0]*v[0]);
		xAux = v[0];

		/* Update v value */
		for (i=0; i<n; i++)
			v[i] = v[i] / xAux;
	}

	return beta;
}

__cudaFunction__
void qrSolver(real *A, real *fjac, real *fvec, int m, int n, real *step, real lambda, real *vAux, real *v) 
{
	int i, j, c, d, k;

	real aux, beta;

	int lmit = m-n+1;

	for (i=0; i<n-1; i++){
		for (j=0; j<lmit; j++){
			A[i*m+j] = fjac[i*lmit+j];
		}
	}

	for (i=0; i<lmit; i++){
		A[m*(n-1)+i] = -fvec[i];
	}

	for (i=lmit; i<m; i++){
		A[m*(n-1)+i] = 0;
	}

	for (i=0; i<n-1; i++){
		for (j=0; j<n-1; j++){
			A[i*m+lmit+j] = 0.0;
		}
		A[i*m+lmit+i] = sqrt(lambda);
	}

	for (i=0; i<n; i++)	
	{
		beta = householder_vector(v, &(A[(i*m)+i]), m-i);

		for (d = i; d < n; d++) {
			for (c = i; c < m; c++) {
				vAux[c] = 0;
	        	for (k = i; k < m; k++) 
	        	{
	        		if (c!=k)
	        		{
	        			aux = -beta * v[c-i] * v[k-i];
	        		}
	        		else
	        		{
	        			aux = 1 + -beta * v[c-i] * v[k-i];
	        		}
	        		
	        		vAux[c] = vAux[c] + aux * A[k+m*d];
	        	}
	      	}
	      	for (c=i; c<m; c++)
	      		A[c+m*d] = vAux[c];
	    }
	}

	/**
	*	Hacer backsustitution sobre la matriz triangular superior
	*/

	int rows = n-1;

	step[rows-1] = A[rows*m+(rows-1)] / A[(rows-1)*m+(rows-1)];

	for (i=rows-2; i>=0; i--){
		step[i] = A[i+m*rows];

		for (j=i+1; j<rows; j++){
			step[i] -= A[j*m + i] * step[j];
		}
		step[i] = step[i] / A[i+m*i];
	}
	
}

__cudaFunction__
real sumSquares(real *fvec, int nObs)
{
	real sum = 0.0;
	int i;

	for (i=0; i<nObs; i++)
	{
		sum += fvec[i] * fvec[i];
	}

	return sum;
}

__cudaFunction__
void computeGradF(real *fgrad, real *fjac, real *fvec, int nObs, int nPrm)
{
    int i, j;
    real sum;
    
    for (i=0; i<nPrm; i++){
        sum = 0;
        for (j=0; j<nObs; j++){
            sum = sum + fjac[i*nObs+j] * fvec[j];
        }
        fgrad[i] = sum;
    }
}

__cudaFunction__
void levmar(real *x, real *xdata, real *ydata, real *residual, int nObs)
{
	int i;

	real qrSolverVAux[NUM_MAX_NOBS+numParams];
	real qrSolverV[NUM_MAX_NOBS+numParams];
	real Aux[(NUM_MAX_NOBS+numParams)*(numParams+1)];	

	int nPrm = numParams;

	real tolX   = 1e-5;
	real tolFun = 1e-5;
	real tolOpt = 1e-4 * tolFun;

	real eps = 2.2204e-16;
	real sqrtEps = sqrt(eps);
    
    real gradF[numParams];

	real fjac[NUM_MAX_NOBS*numParams];
	real fvec[NUM_MAX_NOBS];

	real step[numParams];

	real trialX[numParams];
	real trialFvec[NUM_MAX_NOBS];

	real lambda = 0.01;
    
    int maxFunEvals = 1000;
	int maxIter  = 1000;
	int iters = 1;
    int numFunEvals = 1;
    
	real sumSq, trialSumSq;

	int successfulStep = TRUE;
	int done = FALSE;
    
    real relFactor;
            
	modelo(x, xdata, ydata, nObs, nPrm, fvec, fjac, RESIDUAL);
	sumSq = sumSquares(fvec, nObs);

	modelo(x, xdata, ydata, nObs, nPrm, fvec, fjac, JACOBIAN);
    
    computeGradF(gradF, fjac, fvec, nObs, nPrm);
    
    relFactor = max(norm(gradF,nObs),sqrtEps);

	while (!done) 
	{
		iters++;

		qrSolver(Aux, fjac, fvec, nObs+nPrm, nPrm+1, step, lambda, qrSolverVAux, qrSolverV);

		for (i=0; i<nPrm; i++)
			trialX[i] = x[i] + step[i];

		modelo(trialX, xdata, ydata, nObs, nPrm, trialFvec, fjac, RESIDUAL);
		trialSumSq = sumSquares(trialFvec, nObs);
		
        numFunEvals = numFunEvals + 1;
        
		if (trialSumSq < sumSq)
		{
			for (i=0; i<nObs; i++)
				fvec[i] = trialFvec[i];

			for (i=0; i<nPrm; i++)
				x[i] = trialX[i];

			if (successfulStep)
				lambda = 0.1*lambda;

			modelo(x, xdata, ydata, nObs, nPrm, fvec, fjac, JACOBIAN);
            numFunEvals = numFunEvals + 3;
            
			successfulStep = TRUE;
            
            computeGradF(gradF, fjac, fvec, nObs, nPrm);
            
            if (norm(gradF,nPrm) < tolOpt * relFactor)
            {
                // printf("0");
                done = TRUE;
            }
            else if (norm(step, nPrm) < tolX*(sqrtEps + norm(x,nPrm)))
			{
                // printf("1");
				done = TRUE;
			} 
			else if (absolute(trialSumSq - sumSq) <= tolFun*sumSq) 
			{
                // printf("2");
				done = TRUE;
			}
			else if (iters > maxIter)
			{
                // printf("3");
				done = TRUE;
			}

			sumSq = trialSumSq;
		}
		else
		{
			lambda = 10*lambda;
			successfulStep = FALSE;

			if (norm(step, nPrm) < tolX*(sqrtEps + norm(x,nPrm)))
			{
                // printf("4");
				done = TRUE;
			}
            else if (numFunEvals > maxFunEvals)
            {
                done = TRUE;
            }
		}
        
	}

	*(residual) = sumSq;

}

#define numValidTypes 1

int validDatatypes[numValidTypes] = {DT_INT16};

int isValidDatatype(int datatype)
{
	int i;

	for (i=0; i<numValidTypes; i++){
		if (validDatatypes[i] == datatype){
			return TRUE;
		}
	}
	return FALSE;
}

typedef struct    
{
	real ydata[NUM_MAX_NOBS];
    real solution[numParams];
    real residual;
    int id;
} 
ResultType;

__cudaFunction__
real convertirPixel(int type, int indx, void* dataimage)
{	
	// short
	if (type == DT_INT16)
	{
		short value = ((short *)dataimage)[indx];
	   	return (real)value;
	}

	// raise error?
	// Se supone que esto se ha comrpobado antes..
	return 0.0;
}

const int NUM_THREADS_PER_BLOCK = 150;

const int NUM_MAX_THREADS = 500000;

int main(int argc, char **argv)
{

	struct timeval start, end;
	gettimeofday(&start, NULL);

	/**
	*	Read nitfi file
	*/
	
	int nPrm = numParams;
	int i;

	if (argc == 1) 
	{
		fprintf(stderr, "** No tienes archivo de entrada \n"); return 2;
	}

	nifti_image * nim=NULL;
	char * fin = argv[1];

	//	leer desde linea de comandos los b values
	if (argc == 2)
	{
		fprintf(stderr, "** No tienes valores para bvalues \n"); return 2;
	}
	int numBvalues = atoi(argv[2]);

	if (argc != 3 + numBvalues)
	{
		fprintf(stderr, "** No hay suficientes bvalues. Esperados %d, encontrados %d\n", numBvalues, argc-3); return 2;
	}
	
	real bValues[NUM_MAX_NOBS];

	// leer los valores, pasalos directamente a la referencia de img que se ira al kernel
	for (i=0; i<numBvalues; i++)
	{
		bValues[i] = atoi(argv[3+i]);
	}

	printf("BValues\n");
	for (i=0; i<numBvalues; i++)
	{
		printf("%f ", bValues[i]);
	}
	printf("\n");

	nim = nifti_image_read(fin, 1);
	if (!nim) 
    {
        fprintf(stderr,"** failed to read NIfTI image from '%s'\n", fin); return 2;
    }

    printf("# Archivo nifti correcto '%s'\n", fin);

    /**
    *	Extraer informacion del archivo (i.e numero voxels, tipo de datos...)
    */

	//	Comprobar omp...

	//int ompCores = omp_get_num_procs();

	//printf("# OMP CORES DISPONIBLES %d\n", ompCores);

	//	Asignar los maximos...

	//omp_set_num_threads(ompCores);

    int totalVoxels = 1;

    printf("# Dimensiones: \n");

    // Estos parametros se pueden acceder con otros formatos en el struct
    // del nifti. por ahora lo dejo asi
    // Leer los voxeles, se evita la ultima columna, porque es la 'profundidad'
    for (i = 0; i<nim->ndim-1; i++)
    {
    	printf("\t %d\n", nim->dim[i+1]);
        totalVoxels = totalVoxels * nim->dim[i+1];
    }
    
    int numObservaciones = nim->dim[nim->ndim];
    int imageTotalSize = totalVoxels * numObservaciones;	// Tamanno total de la imagen

    printf("# Numero maximo de threads: %d\n", NUM_MAX_THREADS);
    printf("# Voxels totales: %d\n", totalVoxels);
    printf("# Numero observaciones: %d\n", numObservaciones);
    printf("# Datatype %d\n", nim->datatype);
    printf("# Datatype (String) %s\n", nifti_datatype_string(nim->datatype));
    printf("# Numero de incognitas %d\n", numParams);

    if (numObservaciones != numBvalues)
    {
		fprintf(stderr,"** Hay %d imagenes y %d bvalues. Los valores deben coincidir \n", numObservaciones, numBvalues); return 2;
    }

    // Datos de la imagen para el kernel
    int dx = nim->dim[1];
    int dy = nim->dim[2];
    int dz = nim->dim[3];

    int type = nim->datatype;
    int obs = numObservaciones;

    /**
    *	Comrpobar el tipo de los datos
    */

    if (isValidDatatype(nim->datatype)==FALSE)
    {
    	fprintf(stderr,"** El tipo de datos de la imagen '%s' no se puede procesar (Sin implementar) \n", nifti_datatype_string(nim->datatype)); return 2;
	}

    /**
    *	Mover imagen nifti a la memoria de cuda (TODO: el short ixe esta mal)
    */

    unsigned long int numeroBytesTotalesImage = imageTotalSize * nim->nbyper;

    printf("# Numero de bytes de la imagen %lu\n", numeroBytesTotalesImage);

    // gpuErrchk( cudaMalloc(&dataimage,  imageTotalSize * sizeof(short)) );
    // gpuErrchk( cudaMemcpy(dataimage,nim->data, imageTotalSize * sizeof(short),cudaMemcpyHostToDevice) );

    void * dataimage = nim->data;

	/**
	*	Ejecutar 
	*/

	ResultType * results_CPU = 0;

	results_CPU = (ResultType *) malloc(totalVoxels * sizeof(ResultType));

	// loop

	int localId;

	#pragma omp parallel for private(localId) shared(dx, dy, dz, numObservaciones, type)
	for (localId=0; localId<totalVoxels; localId++)
	{
		int cmp = dx * dy;  // Rango completo de x*y

	    int zi  = localId / (cmp);
	    int lcl = localId - (cmp) * zi;    // quitar la profundidad

	    int xi  = lcl % dx;
	    int yi  = lcl / dy;
		
		//printf("%d (%d, %d, %d) %d (%d, %d, %d) \n", localId, dx, dy, dz, type, xi, yi, zi);

		real ydata[NUM_MAX_NOBS];	// Ahora es short, se canviara eso? no lo cambies, manten siempre la misma
		real value;	// short value;

	    /**
	    *	A partir de las referencias (xi, yi, zi) obtener los valores
	    *	nObs valores de profundidad
	    */

	   	int num_zeros = 0;
		int indx;

		int k;
	    for (k=0; k<numObservaciones; k++)
	    {
	    	indx = xi + (dy*yi) + (dx*dy*zi) + (dx*dy*dz*k);
	    	
			short value = ((short *)dataimage)[indx];
		   	ydata[k] = (real)value;

	    	if (value == 0.0)
	    		num_zeros = num_zeros + 1;
	    }
		
	    if (num_zeros > 3)
	    {
	    	for (k=0; k<nPrm; k++) 
	        {
	        	results_CPU[localId].solution[k] = 0;
	        }
	       	results_CPU[localId].residual = 0;
	    }
	    else
	    {

			real x[numParams];
			initialize_params(x);

			real *xdata = bValues;

			real residual = 0;

			levmar(x, xdata, ydata, &residual, numObservaciones);
			
			//	(ADC ANALITICO.... en lugar de levmar)
			//x[0] = log(ydata[0] / ydata[1]) / (xdata[1] - xdata[0]);

			for (k=0; k<numObservaciones; k++) 
			{
				results_CPU[localId].ydata[k] = ydata[k];
			}

			// Pasar valores
			for (k=0; k<nPrm; k++)
			{
				results_CPU[localId].solution[k] = x[k];
			}
			results_CPU[localId].residual = residual;
		}

		// mostrar
		//printf("%d %f %f %f\n", localId, results_CPU[localId].solution[0], results_CPU[localId].solution[1], results_CPU[localId].solution[2]);
	}

    printf("D0ne!\n");

    // Para el timer y mostrar

	gettimeofday(&end, NULL);

	float delta = ((end.tv_sec  - start.tv_sec) * 1000000u + 
	         end.tv_usec - start.tv_usec) / 1.e6;

    printf("tiempo computo %f\n", delta);

    // una salida mas
    // 

    int j;
    for (i=0; i<totalVoxels; i++)
    {
		//	ydata
		for (j=0; j<numObservaciones; j++)
		{
			printf("%f ", results_CPU[i].ydata[j]);
		}

		//	resultados
        for (j=0; j<nPrm; j++)  
        {
        	printf("%f ", results_CPU[i].solution[j]);
        }
        printf("\n");
    }

	//	Los datos salen de forma lineal, asi que deberian convertirse a volumen 3D con Matlab por ejemplo (reshape)

    nifti_image_free( nim );
}
