
#include "solver.h"
#include <math.h>

__cudaFunction__
void modelo(real *x, real *xdata, real *ydata, volatile int nObs, int nPrm, real *fvec, real *fjac, int flag) 
{
	int i;

	if (flag == JACOBIAN)
	{
		for (i = 0; i < nObs; i++) 
		{
			fjac[i+((nObs)*0)] = -xdata[i] * ydata[0] * ( exp(-xdata[i]*x[0]) );
		}
	}
	
	if (flag == RESIDUAL)
	{
		for (i = 0; i < nObs; i++) 
		{
			fvec[i] = (ydata[0] * ( exp(-xdata[i]*x[0]) ) ) - ydata[i];
		}
	}
}

__cudaFunction__
void initialize_params(real *x)
{
	x[0] = 1e-3;
}

