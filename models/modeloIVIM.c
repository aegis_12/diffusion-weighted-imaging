
#include <solver.h>
#include <math.h>

__cudaFunction__
void modelo(real *x, real *xdata, real *ydata, volatile int nObs, int nPrm, real *fvec, real *fjac, int flag) 
{
	int i;

	if (flag == JACOBIAN)
	{
		for (i = 0; i < nObs; i++) 
		{
			fjac[i+((nObs)*0)] = ydata[0] * (xdata[i] * exp(-x[0] * xdata[i]) * (x[2] - 1) - xdata[i] * x[2] * exp(-xdata[i] * (x[0] + x[1])));
			fjac[i+((nObs)*1)] = -xdata[i] * ydata[0] * x[2] * exp(-xdata[i] * (x[0] + x[1]));
			fjac[i+((nObs)*2)] = ydata[0] * (exp(-xdata[i] * (x[0] + x[1])) - exp(-x[0] * xdata[i]));
		}
	}
	
	if (flag == RESIDUAL)
	{
		for (i = 0; i < nObs; i++) 
		{
			fvec[i] = (ydata[0] * ( (1-x[2]) * exp(-xdata[i]*x[0]) + x[2] * exp(-xdata[i]*(x[0]+x[1])) ) ) - ydata[i];
		}
	}
}

__cudaFunction__
void initialize_params(real *x)
{
	x[0] = 0.0010;
	x[1] = 0.0500;
	x[2] = 0.3000;	
}
